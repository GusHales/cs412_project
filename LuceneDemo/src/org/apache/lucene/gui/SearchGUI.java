package org.apache.lucene.gui;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import org.apache.lucene.index.IndexFilesJavaFX;
import org.apache.lucene.model.SearchFilesJavaFX;


public class SearchGUI extends Application {

	public final static SearchFilesJavaFX searchFX = new SearchFilesJavaFX();
	public final static IndexFilesJavaFX indexFX = new IndexFilesJavaFX();
	public static Label searchResults = new Label("");
	public final static CheckBox stemming = new CheckBox("Stemming");
	public final static CheckBox stopWordsCB = new CheckBox("Stopwords");
	private ScrollPane searchRes = new ScrollPane();
	private Label numberOfWords = new Label("Number of Words");
	private static HashMap<String, Boolean> options = new HashMap<String, Boolean>();

	public static void main(String[] args) {
		initOptions();
		launch(SearchGUI.class, args);
	}

	@Override
	public void start(final Stage stage) {
		// Use a border pane as the root for scene
		BorderPane border = new BorderPane();

		//set all panes
		HBox hbox = addHBox(stage);
		border.setTop(hbox);
		border.setLeft(addLeftFlowPane(stage));
		border.setCenter(addVBox());
		border.setRight(addFlowPane());
		
		Scene scene = new Scene(border);

		stage.setScene(scene);
		stage.setTitle("Search System");
		stage.setHeight(550);
		stage.show();
	}

	/*
	 * Creates an HBox where the search button and the text field will reside.
	 */
	private HBox addHBox(final Stage stage) {
		final List<Hyperlink> removeLinks = new ArrayList<Hyperlink>();
		HBox hbox = new HBox();
		hbox.setPadding(new Insets(15, 12, 15, 100));
		hbox.setSpacing(10);   // Gap between nodes
		hbox.setStyle("-fx-background-color: #E0EFF1;");

		Button buttonSearch = new Button("Search");
		buttonSearch.setPrefSize(100, 20);

		final TextField userTextField = new TextField();
		userTextField.setPrefSize(420, 20);

		ObservableList<String> avOptions = 
				FXCollections.observableArrayList(
						"Select All",
						"Java Language Reference",
						"Java in a Nutshell",
						"Java Fundamental Classes Reference",
						"Exploring Java",
						"Java AWT Reference"
						);

		final ComboBox<String> comboBox = new ComboBox<String>(avOptions);
		comboBox.setValue("Select All");
		comboBox.setPrefSize(150,20);


		hbox.getChildren().addAll(userTextField, buttonSearch, comboBox);

		buttonSearch.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				VBox content = new VBox();

				try {

					List<Hyperlink> links = new ArrayList<Hyperlink>();
					links = searchFX.getResults(userTextField.getText(), comboBox.getValue(), options);
					if(links == null) {
						numberOfWords.setText(searchFX.returnStringBuilder());;
					}
					else{
						removeLinks.addAll(links);
						for(Hyperlink r : removeLinks){
							getSearchResults().setContent(r);
						}
						for(final Hyperlink l : links){
							content.getChildren().add(l);	
							System.out.println(l);
							l.setOnAction(new EventHandler<ActionEvent>(){
								@Override
								public void handle(ActionEvent arg0) {
									if(options.get("levenshtein")){
										userTextField.setText(l.getText());
									} else {
										getHostServices().showDocument("file://".concat(l.getText()));
									}			
								}				
							});
						}	
						numberOfWords.setText(searchFX.returnStringBuilder());;
						searchRes.setContent(content);
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (org.apache.lucene.queryparser.classic.ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});      
		return hbox;
	}

	/*
	 * Creates a VBox with a list of links for the center region
	 */
	private VBox addVBox() {

		VBox vbox = new VBox();
		vbox.setPadding(new Insets(10)); // Set all sides to 10
		vbox.setSpacing(8);              // Gap between nodes

		Text title = new Text("Search Results");
		title.setFont(Font.font("Arial", FontWeight.BOLD, 14));
		vbox.getChildren().add(title);

		searchResults = new Label("results here");
		vbox.setPrefSize(450, 200);

		searchRes.setPrefSize(450, 350);

		vbox.getChildren().addAll(searchRes, numberOfWords);

		return vbox;
	}

	public ScrollPane getSearchResults() {
		return searchRes;
	}

	public void setSearchResults(Hyperlink link) { 
		searchRes.setContent(link);
	}

	/*
	 * Creates a right flow pane with checkboxes
	 */
	private FlowPane addFlowPane() {

		FlowPane flow = new FlowPane();
		flow.setPadding(new Insets(5, 0, 5, 0));
		flow.setVgap(4);
		flow.setHgap(4);
		flow.setPrefWrapLength(170); 
		flow.setStyle("-fx-background-color: #7DB4B5;");


		Label advancedOptions = new Label("Advanced Options");
		advancedOptions.setUnderline(true);
		advancedOptions.setPadding(new Insets(1,1,1,18));

		ToggleGroup group = new ToggleGroup();

		CheckBox leviDistance = new CheckBox("Levenshtein distance");
		leviDistance.selectedProperty().addListener(
				new ChangeListener<Boolean>() {

					@Override
					public void changed(ObservableValue<? extends Boolean> observable,	Boolean oldVal, Boolean newVal) {
						setOption("levenshtein", newVal);
					}
				});
		
		leviDistance.setPadding(new Insets(6));

		flow.getChildren().addAll(advancedOptions, leviDistance);

		return flow;
	}

	/*
	 * Creates right pane to store what ever we are going to have here, it can be removed if its not used.
	 */
	private FlowPane addLeftFlowPane(final Stage stage) {
		FlowPane flow = new FlowPane();
		flow.setPadding(new Insets(30, 12, 15, 50));
		flow.setVgap(20);
		flow.setHgap(20);
		flow.setPrefWrapLength(170);
		flow.setStyle("-fx-background-color: #7DB4B5;");

		stemming.setIndeterminate(false);
		stemming.setPadding(new Insets(6));
		stemming.selectedProperty().addListener(new ChangeListener<Boolean>() {
			public void changed(ObservableValue<? extends Boolean> ov,
					Boolean old_val, Boolean new_val) {
				stemming.setSelected(new_val);
			}
		});

		stopWordsCB.setIndeterminate(false);
		stopWordsCB.setPadding(new Insets(6));
		stopWordsCB.selectedProperty().addListener(new ChangeListener<Boolean>() {
			public void changed(ObservableValue<? extends Boolean> ov,
					Boolean old_val, Boolean new_val) {
				stopWordsCB.setSelected(new_val);
			}
		});

		Button buttonIndex = new Button("Index Files");
		buttonIndex.setPrefSize(100, 20);
		buttonIndex.setOnAction(new EventHandler<ActionEvent>() {

			final DirectoryChooser indexPath = new DirectoryChooser();
			final DirectoryChooser docPath = new DirectoryChooser();

			@Override
			public void handle(ActionEvent e) {
				indexPath.setTitle("Select index folder");
				docPath.setTitle("Path to data");
				File fileIndex = indexPath.showDialog(null);
				File filePath = docPath.showDialog(null);

				if (fileIndex != null || filePath != null) {
					System.out.println("Index path = "+fileIndex.getAbsoluteFile());
					searchFX.setIndex(fileIndex.getAbsolutePath());
					boolean stemmingBool, stopwordBool;
					stemmingBool = false;
					stopwordBool = false;
					if(stemming.isSelected()){
						stemmingBool = true;
					}
					else if(stopWordsCB.isSelected()) {
						stopwordBool = true;
					}
					indexFX.doIndex(stemmingBool, stopwordBool, fileIndex.getAbsolutePath(), filePath.getAbsolutePath());
				}
				if (filePath != null) {
					System.out.println("Doc path = "+filePath.getAbsolutePath());
				}
			}
		});

		flow.getChildren().addAll(buttonIndex, stemming, stopWordsCB);

		return flow;
	}
	
	/**
	 * Set all options to be initially false
	 */
	private static void initOptions()
	{
		options.put("stopwords", false);
		options.put("hits", false);
		options.put("levenshtein", false);
	}
	
	/**
	 * Set an option as turned on or off (true or false)
	 * @param optionName
	 * @param value
	 */
	private void setOption(String optionName, Boolean value) {
		options.put(optionName, value);
	}
	
}