package org.apache.lucene.model;

/************
 * AJ 17/12/14
 * Class for getting other possible search terms with a small edit distance from what the user 
 * originally searched.
 */

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class LevenshteinTest {

	static ArrayList<String> dictionary = new ArrayList<String>();

	public static void main(String[] args) {
		readDictionary();
		
		//Print out all the close matches
		//for(String word: matches("hava", 1)) Commented cos it's complaining matches() isn't static.
		//	System.out.println(word.toString());
	}
	
	public LevenshteinTest()
	{
		readDictionary();
	}

	private static void readDictionary() {
		File dictionaryFile = new File(
				"./sortedList.txt");
		try {
			FileReader fr = new FileReader(dictionaryFile);
			Scanner dictionaryReader = new Scanner(fr);
			while (dictionaryReader.hasNext()) {
				dictionary.add(dictionaryReader.nextLine());
			}
			dictionaryReader.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Get all words that are close matches to <code>input</code>
	 * @param input - The String we want matches for
	 * @param boundary - How large the Levenshtein distance can be. Values larger than 3 are not advised.
	 * @return The Set of matches.
	 */
	public Set<String> matches(String input, int boundary) {
		int dist = -1;
		HashSet<String> matches = new HashSet<String>();
		for (String word : dictionary) {
			if (Math.abs(word.length() - input.length()) <= boundary) {
				dist = calcDistance(input, word);
				if (dist <= boundary)
					matches.add(word);
			}
		}

		return matches;
	}

	/**
	 * Calculate the Levenshtein distance between <code>input</code> and <code>possMatch</code>.
	 * @param input
	 * @param possMatch
	 * @return An int of the distance.
	 */
	private static int calcDistance(String input, String possMatch) {
		int[][] distance = new int[input.length() + 1][possMatch.length() + 1];

		for (int i = 0; i <= input.length(); i++)
			distance[i][0] = i;
		for (int j = 1; j <= possMatch.length(); j++)
			distance[0][j] = j;

		for (int i = 1; i <= input.length(); i++)
			for (int j = 1; j <= possMatch.length(); j++)
				distance[i][j] = minimum(
						distance[i - 1][j] + 1,
						distance[i][j - 1] + 1,
						distance[i - 1][j - 1]
								+ ((input.charAt(i - 1) == possMatch
										.charAt(j - 1)) ? 0 : 1));

		return distance[input.length()][possMatch.length()];
	}

	private static int minimum(int a, int b, int c) {
		return Math.min(Math.min(a, b), c);
	}
}
