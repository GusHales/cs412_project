
package org.apache.lucene.model;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javafx.scene.control.Hyperlink;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

/** Simple command-line based search demo. */
public class SearchFilesJavaFX {

	private String index = "";

	static StringBuilder searchResults = new StringBuilder();
	public SearchFilesJavaFX() {

	}

	@SuppressWarnings("deprecation")
	public List<Hyperlink> getResults(String search, String selectedBook, HashMap<String, Boolean> searchOptions) throws IOException, ParseException{
		searchResults.setLength(0);
		String field = "contents";
		String queries = null;
		int repeat = 0;
		boolean raw = false;
		String queryString = null;
		int hitsPerPage = 10;

		if(index.isEmpty()) {
			searchResults.append("Please index data.");
		} else {
			IndexReader reader = DirectoryReader.open(FSDirectory.open(new File(index)));
			IndexSearcher searcher = new IndexSearcher(reader);
			Analyzer analyzer = null;
			if(!searchOptions.get("stopwords")) {
				analyzer = new StandardAnalyzer(Version.LUCENE_40,CharArraySet.EMPTY_SET);
			} else if (searchOptions.get("stemming")) {
			  analyzer = new EnglishAnalyzer(Version.LUCENE_40);
			} else if (!searchOptions.get("stopwords") && searchOptions.get("stemming")) {
			  analyzer = new EnglishAnalyzer(Version.LUCENE_40,CharArraySet.EMPTY_SET);
			}
			 else {
				 analyzer = new StandardAnalyzer(Version.LUCENE_40);
			}
			QueryParser parser = new QueryParser(Version.LUCENE_40, field, analyzer);
			String line = search;

			if (line == null || line.length() == -1) {
				searchResults.append("Query string is empty");
			}

			line = line.trim();
			if (line.length() == 0) {
				searchResults.append("Query string is empty");
			}
			Query query = null;
			if (line.equals("")) {
			} else {
				query = parser.parse(line);
				searchResults.append("Searching for: " + query.toString(field));

				if (repeat > 0) { // repeat & time as benchmark
					Date start = new Date();
					for (int i = 0; i < repeat; i++) {
						searcher.search(query, null, 100);
					}
					Date end = new Date();
					searchResults.append("\nTime: "
							+ (end.getTime() - start.getTime()) + "ms");
				}
				String bookSearch = getBook(selectedBook);
				return doPagingSearch(bookSearch, search, searcher, query,
						hitsPerPage, raw, queries == null && queryString == null, searchOptions);
			}
		}
		//		if (queryString != null) {
		//			return "queryString != null";
		//		}
		//		return line;
		return null;

	}

	/**
	 * This demonstrates a typical paging search scenario, where the search engine presents 
	 * pages of size n to the user. The user can then go to the next page if interested in
	 * the next hits.
	 * 
	 * When the query is executed for the first time, then only enough results are collected
	 * to fill 5 result pages. If the user wants to page beyond this limit, then the query
	 * is executed another time and all hits are collected.
	 * 
	 */
	public static List<Hyperlink> doPagingSearch(String bookSearch, String in, IndexSearcher searcher, Query query, int hitsPerPage, boolean raw, boolean interactive, HashMap<String, Boolean> searchOptions) throws IOException {
		LevenshteinTest lev = new LevenshteinTest();
		// Collect enough docs to show 5 pages
		TopDocs results = searcher.search(query, 5 * hitsPerPage);
		ScoreDoc[] hits = results.scoreDocs;
		List<Hyperlink> links = new ArrayList<Hyperlink>();
		links.clear();
		int numTotalHits = results.totalHits;
		int numberOfResults = 15;
		if(numberOfResults > hits.length ){
			numberOfResults = hits.length -1;
		}
		searchResults.append("\n" + numTotalHits + " total matching documents");   

		for (int i = 0; i < (hits.length==0 ? 15 : numberOfResults); i++) {
			if (raw) {                              // output raw format
				searchResults.append("\ndoc="+hits[i].doc+" score="+hits[i].score);
				continue;
			}
			if (results.totalHits == 0) {
				String noResultsText = "No documents had a match to that search term";
				if (searchOptions.get("levenshtein")) 
				{
					HashSet<String> matches = (HashSet<String>) lev.matches(query.toString().split("\\s|:")[1], 1);
					
					if(!matches.isEmpty())
					{
						// AJ 18/12/14: Horrible lines that get all the words with
						// small edit distance for the first query term.
						noResultsText = noResultsText.concat("\nDid you mean one of these: ");
						
						for(String match : matches)
						{
							Hyperlink link = new Hyperlink();
							link.setText(match);
							links.add(link);
							noResultsText = noResultsText.concat(match + ", ");
						}
						//AJ 18/12/14: Warning: Hacky as balls! But it works to remove the last extra comma..
						noResultsText = noResultsText.substring(0,noResultsText.length()-2).concat("?");
					}
					return links;
				}
			} else {
				Document doc = searcher.doc(hits[i].doc);

				//System.out.println(results.totalHits);
				String path = doc.get("path");

				Hyperlink link = new Hyperlink(path);

				if(bookSearch.equals("SelectAll")){
					links.add(link);
					//					searchResults.append("\n" + (i + 1) + ". " + path);
					//					String title = doc.get("title");
					//					if (title != null) {
					//						searchResults.append("   Title: " + doc.get("title"));
					//					}
				}else if(path.contains(bookSearch)){
					//					searchResults.append("\n" + (i + 1) + ". " + path);
					//					String title = doc.get("title");
					//					if (title != null) {
					//						searchResults.append("   Title: " + doc.get("title"));
					//					}
					links.add(link);
				}else{

				}
			}
		}
		//return searchResults.toString();
		return links;
	}

	public String getBook(String bookName){

		switch (bookName){
		case "Java Language Reference":
			return "langref";
		case "Java in a Nutshell":
			return "javanut";
		case "Java Fundamental Classes Reference":
			return "fclass";
		case "Exploring Java":
			return "exp";
		case "Java AWT Reference":
			return "awt";
		case "Select All":
			return "SelectAll";
		}

		return "";
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getIndex() {
		return this.index;
	}

	public String returnStringBuilder() {
		return searchResults.toString();
	}
}

//package org.apache.lucene.demo;
//
///*
// * Licensed to the Apache Software Foundation (ASF) under one or more
// * contributor license agreements.  See the NOTICE file distributed with
// * this work for additional information regarding copyright ownership.
// * The ASF licenses this file to You under the Apache License, Version 2.0
// * (the "License"); you may not use this file except in compliance with
// * the License.  You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//
//import java.io.File;
//import java.io.IOException;
//import java.util.Date;
//
//import org.apache.lucene.analysis.Analyzer;
//import org.apache.lucene.analysis.en.EnglishAnalyzer;
//import org.apache.lucene.analysis.standard.StandardAnalyzer;
//import org.apache.lucene.document.Document;
//import org.apache.lucene.index.DirectoryReader;
//import org.apache.lucene.index.IndexReader;
//import org.apache.lucene.queryparser.classic.ParseException;
//import org.apache.lucene.queryparser.classic.QueryParser;
//import org.apache.lucene.search.IndexSearcher;
//import org.apache.lucene.search.Query;
//import org.apache.lucene.search.ScoreDoc;
//import org.apache.lucene.search.TopDocs;
//import org.apache.lucene.store.FSDirectory;
//import org.apache.lucene.util.Version;
//
///** Simple command-line based search demo. */
//public class SearchFilesJavaFX {
//
//	static StringBuilder searchResults = new StringBuilder();
//	private String index = ""; 
//	public SearchFilesJavaFX() {
//
//	}
//
//	public String getResults(String search) throws IOException, ParseException{
//		if(index.isEmpty()) {
//			setIndex("//Users//gushales//Documents//University//4thYear//CS412//index2");
//		}
//		searchResults.setLength(0);
//		String field = "contents";
//		String queries = null;
//		int repeat = 0;
//		boolean raw = false;
//		String queryString = null;
//		int hitsPerPage = 10;
//		
//		QueryParser parser = null;
//
//		IndexReader reader = DirectoryReader.open(FSDirectory.open(new File(index)));
//		IndexSearcher searcher = new IndexSearcher(reader);
//		Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);
//		parser = new QueryParser(Version.LUCENE_40, field, analyzer);		
//		String line = search;
//		
//		if (line == null || line.length() == -1) {
//			return "Query string is empty";
//		}
//
//		line = line.trim();
//		if (line.length() == 0) {
//			return "Query string is empty";
//		}
//
//		Query query = parser.parse(line);
//		searchResults.append("\nSearching for: " + query.toString(field));
//
//		if (repeat > 0) { // repeat & time as benchmark
//			Date start = new Date();
//			for (int i = 0; i < repeat; i++) {
//				searcher.search(query, null, 100);
//			}
//			Date end = new Date();
//			searchResults.append("\nTime: " + (end.getTime() - start.getTime())
//					+ "ms");
//		}
//
//		return doPagingSearch(search, searcher, query, hitsPerPage, raw, queries == null && queryString == null);
//	}
//
//	/**
//	 * This demonstrates a typical paging search scenario, where the search engine presents 
//	 * pages of size n to the user. The user can then go to the next page if interested in
//	 * the next hits.
//	 * 
//	 * When the query is executed for the first time, then only enough results are collected
//	 * to fill 5 result pages. If the user wants to page beyond this limit, then the query
//	 * is executed another time and all hits are collected.
//	 * 
//	 */
//	public static String doPagingSearch(String in, IndexSearcher searcher, Query query, int hitsPerPage, boolean raw, boolean interactive) throws IOException {
//
//		// Collect enough docs to show 5 pages
//		TopDocs results = searcher.search(query, 5 * hitsPerPage);
//		ScoreDoc[] hits = results.scoreDocs;
//
//		int numTotalHits = results.totalHits;
//		searchResults.append("\n" + numTotalHits + " total matching documents");    
//		for (int i = 0; i < 10; i++) {
//			if (raw) {                              // output raw format
//				searchResults.append("\ndoc="+hits[i].doc+" score="+hits[i].score);
//				continue;
//			}
//			if (results.totalHits == 0) {
//				return "No documents had a match to that search term";
//			} else {
//				Document doc = searcher.doc(hits[i].doc);
//				String path = doc.get("path");
//				searchResults.append("\n" + (i + 1) + ". " + path);
//				String title = doc.get("title");
//				if (title != null) {
//					searchResults.append("   Title: " + doc.get("title"));
//				}
//			}
//		}
//		return searchResults.toString();
//	}
//}
